import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Task29';
  public message: string = 'This is my first Angular ever! YAY!';
  public data: any[] = [{
    name: 'Robert',
    surname: 'Björck',
    age: '0b100100 in binary'
  },
  {
    name: 'Dolph',
    surname: 'Lundgren',
    age: '62 badass years!'
  },
  {
    name: 'Arnold',
    surname: 'Schwarzenegger',
    age: '72'
  },
  {
    name: 'Sylvester',
    surname: 'Stallone',
    age: '73'
  }

  ];
}
